package buet.touhiDroid.MLOffline1.models;

import buet.touhiDroid.MLOffline1.utils.Lg;

import java.util.ArrayList;

/**
 * Created by touhid on 10/3/15.
 *
 * @author touhid
 */
public class DecisionTree {

    private static final int CLS_BENIGN = 0;
    private static final int CLS_MALIGNANT = 1;

    private ArrayList<Character> attrList;

    private ArrayList<PatientDataItem> trainDataList;
    private double totalEntropy;
    private int label = -1; // the label
    public ArrayList<DecisionTree> childDecTrees;
    private char decidedAttribute;
    private char parentAttrib;

    public DecisionTree(char parentAttrib, ArrayList<PatientDataItem> trainDataList, ArrayList<Character> attrList) {
        this.trainDataList = trainDataList;
        this.childDecTrees = new ArrayList<DecisionTree>();
        this.attrList = attrList;
        this.parentAttrib = parentAttrib;
        this.label = -1;
        /*if(trainDataList.isEmpty())
            label = getLabel();*/
        runID3();
    }

    public int getLabel() {
        return label;
    }

    public char getDecidedAttribute() {
        return decidedAttribute;
    }

    public char getParentAttrib() {
        return parentAttrib;
    }

    public ArrayList<DecisionTree> getChildDecTrees() {
        return childDecTrees;
    }

    private void runID3() {
        if (trainDataList.isEmpty()) {
            label = getMostCommonValue();
        } else {

            totalEntropy = getEntropy(trainDataList);
            label = calcLabel();
            if (label == -1) {
                decidedAttribute = getBestAttribute();
                for (int i = 1; i < 11; i++) {
                    ArrayList<PatientDataItem> temp = new ArrayList<PatientDataItem>();
                    for (PatientDataItem aTrainDataList : trainDataList)
                        if (aTrainDataList.getAttrNo(decidedAttribute) == i) {
                            temp.add(aTrainDataList);
                        }

                    ArrayList<Character> updatedAttrList = new ArrayList<Character>();
                    for (Character anAttrList : attrList)
                        if (!anAttrList.equals(decidedAttribute)) {
                            updatedAttrList.add(anAttrList);
                        }
                    childDecTrees.add(new DecisionTree(decidedAttribute, temp, updatedAttrList));
                }
            }
            /*if(label<0)
                label = getLabel();*/
        }
    }

    public char getBestAttribute() {
        double gain;
        char attr = attrList.get(0);
        gain = getInformationGain(attrList.get(0));

        for (Character anAttrList : attrList) {

            if (gain < getInformationGain(anAttrList)) {
                gain = getInformationGain(anAttrList);
                attr = anAttrList;
            }
        }
        return attr;
    }

    public int calcLabel() {
        boolean flag = false;
        for (PatientDataItem aTrainDataList : trainDataList) {
            if (aTrainDataList.getCls() == 1) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            label = 0;
            decidedAttribute = '\0';
        } else {

            boolean flg = false;
            for (PatientDataItem aTrainDataList : trainDataList) {
                if (aTrainDataList.getCls() == 0) {
                    flg = true;
                    break;
                }
            }

            if (!flg) {
                label = 1;
                decidedAttribute = '\0';
            } else {

                if (attrList.isEmpty()) {
                    decidedAttribute = '\0';
                    this.label = getMostCommonValue();
                }
            }
        }
        return label;
    }

    public int getMostCommonValue() {

        int val;
        int zeroCount = 0;
        int oneCount = 0;
        for (PatientDataItem aTrainDataList : trainDataList) {
            if (aTrainDataList.getCls() == 0) zeroCount++;
            else oneCount++;
        }
        // Lg.pl("0=" + zeroCount + ", 1=" + oneCount);
        if (zeroCount > oneCount) {
            val = 0;
        } else {
            val = 1;
        }
        return val;
    }

    private double getEntropy(ArrayList<PatientDataItem> dataList) {
        double entropy = 0;
        int p[] = {0, 0};
        for (PatientDataItem aTrainList : dataList) {
            if (aTrainList.getCls() == 0) p[0]++;
            else p[1]++;
        }
        int dtSz = dataList.size();
        if (p[0] == dtSz || p[1] == dtSz)
            entropy = 0.0;
        else
            entropy = -(((double) p[0] / (double) dtSz)
                    * (Math.log((double) p[0] / (double) dtSz) / Math.log(2.0)))
                    - (((double) p[1] / (double) dtSz)
                    * (Math.log((double) p[1] / (double) dtSz) / Math.log(2.0)));

        return entropy;
    }

    private double getInformationGain(char attrCh) {
        double val;
        double sum = 0;
        int count[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ArrayList<ArrayList<PatientDataItem>> list = new ArrayList<ArrayList<PatientDataItem>>();
        for (int i = 0; i < 10; i++)
            list.add(new ArrayList<PatientDataItem>());

        for (PatientDataItem aTrainDataList : trainDataList) {
            count[aTrainDataList.getAttrNo(attrCh) - 1]++;
            list.get(aTrainDataList.getAttrNo(attrCh) - 1).add(aTrainDataList);
        }

        for (int i = 0; i < 10; i++) {
            sum += (((double) list.get(i).size()
                    / (double) trainDataList.size()) * getEntropy(list.get(i)));
        }

        val = this.totalEntropy - sum;
        return val;
    }
}
