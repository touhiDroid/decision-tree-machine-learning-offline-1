package buet.touhiDroid.MLOffline1.models;

import java.util.Arrays;

/**
 * Created by touhid on 10/3/15.
 * @author touhid
 */
public class PatientDataItem {

    private static final int ID_POS = 0;
    private static final int CLS_POS = 9;

    // 1st int is the ID & the last (10th) int is the class (benign-0/malignant-1) indicator
    private int id, cls;
    // Attributes : 10 in this project, but now kept to support any robustness later
    private int attr[];

    public PatientDataItem(int[] attr) {
        this.attr = attr;
        this.id = attr[ID_POS];
        this.cls = attr[CLS_POS];
    }

    public int getId() {
        return id;
    }

    public int getCls() {
        return cls;
    }

    public int[] getAttr() {
        return attr;
    }

    public int getAttrNo(char attrChar) {
        int attrNo = attrChar - 'a';
        return attr[attrNo];
    }

    public void setId(int id) {
        this.id = id;
        this.attr[ID_POS] = id;
    }

    public void setCls(int cls) {
        this.cls = cls;
        this.attr[CLS_POS] = cls;
    }

    public void setAttr(int[] attr) {
        this.attr = attr;
    }

    @Override
    public String toString() {
        return "PatientDataItem{" +
                "id=" + id +
                ", cls=" + cls +
                ", attr=" + Arrays.toString(attr) +
                '}';
    }
}
