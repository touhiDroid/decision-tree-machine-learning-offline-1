package buet.touhiDroid.MLOffline1;

import buet.touhiDroid.MLOffline1.models.DecisionTree;
import buet.touhiDroid.MLOffline1.models.PatientDataItem;
import buet.touhiDroid.MLOffline1.utils.Lg;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by touhid on 10/3/15.
 *
 * @author touhid
 */
public class CancerDiagnoser {

    public static void main(String[] args) {

        double accuracy = 0.0f, accN = 0.0f, accP = 0.0f,
                precision = 0.0f, recall = 0.0f,
                gMean = 0.0f, fMeasure = 0.0f;

        ArrayList<PatientDataItem> patDataItems = inputAllPatientData(args);
        int totalDataCount = patDataItems.size();

        for (int nRun = 0; nRun < 100; nRun++) {
            Collections.shuffle(patDataItems);

            int trainDataLength = (int) (totalDataCount * 0.80);
            ArrayList<PatientDataItem> trainList = getTrainDataList(
                    patDataItems, trainDataLength);
            ArrayList<PatientDataItem> testList = getTestDataList(
                    patDataItems, totalDataCount, trainDataLength);

            int tp = 0, tn = 0, fp = 0, fn = 0;
            ArrayList<Character> attrList = new ArrayList<Character>();
            for (char c = 'a'; c < 'j'; c++)
                attrList.add(c);

            // TODO Consider weighted samples
            DecisionTree decTree = new DecisionTree('0', trainList, attrList);
            // TODO Boosting calculation

            // printTreeStatus(decTree);

            for (PatientDataItem patientDataItem : testList) {
                DecisionTree dest = decTree;
                int label = -1;
                while (true) {
                    label = dest.getLabel();//>-1?dest.get MostCommonClass():label;
                    if (dest.getChildDecTrees().isEmpty())
                        break;
                    dest = dest.getChildDecTrees().get(
                            patientDataItem.getAttrNo(dest.getDecidedAttribute()) - 1
                    );// FIX_ME -1?
                }
                if (patientDataItem.getCls() == 0 && label == 0) tp++;
                else if (patientDataItem.getCls() == 1 && label == 1) tn++;
                else if (patientDataItem.getCls() == 0 && label == 1) fn++;
                else if (patientDataItem.getCls() == 1 && label == 0) fp++;
                Lg.pl("Test case-" + (nRun + 1)
                        + " :: Found Class=" + label + " , Actual-class=" + patientDataItem.getCls());/**/
            }
            accuracy += (((double) (tp + tn) / (double) (tp + tn + fp + fn)) * 100.0);
            accN = (double) tn / (double) (tn + fp);
            accP = (double) tp / (double) (tp + fn);
            gMean += (Math.sqrt(accN * accP) * 100.0);
            precision += (((double) tp / (double) (tp + fp)) * 100.0);
            recall += (accP * 100.0);
            fMeasure += (((2 * (((double) tp / (double) (tp + fp))) * accP) / ((((double) tp / (double) (tp + fp))) + accP)) * 100.0);

            trainList.clear();
            testList.clear();
        }
        Lg.pl("Accuracy : " + accuracy / 100.0 + "%");
        Lg.pl("Precision : " + precision / 100.0 + "%");
        Lg.pl("Recall : " + recall / 100.0 + "%");
        Lg.pl("F-Measure : " + fMeasure / 100.0 + "%");
        Lg.pl("G-Mean : " + gMean / 100.0 + "%");
    }

    private static ArrayList<PatientDataItem> getTestDataList(ArrayList<PatientDataItem> patDataItems,
                                                              int totalDataCount, int trainDataLength) {
        ArrayList<PatientDataItem> testList = new ArrayList<PatientDataItem>();
        for (int i = trainDataLength; i < totalDataCount; i++)
            testList.add(patDataItems.get(i));
        return testList;
    }

    private static ArrayList<PatientDataItem> getTrainDataList(ArrayList<PatientDataItem> patDataItems,
                                                               int trainDataLength) {
        ArrayList<PatientDataItem> trainList = new ArrayList<PatientDataItem>();
        for (int i = 0; i < trainDataLength; i++)
            trainList.add(patDataItems.get(i));
        return trainList;
    }

    //region Commented-out Logging codes
    // main()
        /*Lg.pl("Total data count: " + totalDataCount);
        Lg.pl("First item: " + patDataItems.get(0).toString());
        Lg.pl("Last item: " + patDataItems.get(totalDataCount - 1).toString());*/
        /*Lg.pl("Train data: " + trainList.size());
        Lg.pl("Test data: " + testList.size());
        Lg.pl("First train-data: " + trainList.get(0).toString());
        Lg.pl("Last test-data: " + testList.get(testList.size() - 1).toString());*/
    //endregion

    private static ArrayList<PatientDataItem> inputAllPatientData(String inpFileName[]) {
        Scanner scanner;
        try {
            scanner = new Scanner(new File(inpFileName[0]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            scanner = new Scanner(System.in);
        }

        ArrayList<PatientDataItem> patDataItems = new ArrayList<PatientDataItem>();
        while (scanner.hasNextLine()) {
            String dataLine = scanner.nextLine();
            StringTokenizer dataTokens = new StringTokenizer(dataLine, ",");

            int attr[] = new int[10];
            int i = 0;
            while (dataTokens.hasMoreTokens()) {
                attr[i++] = Integer.parseInt(dataTokens.nextToken().trim());
            }
            patDataItems.add(new PatientDataItem(attr));
        }
        return patDataItems;
    }

}
