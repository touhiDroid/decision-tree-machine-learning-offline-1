package buet.touhiDroid.MLOffline1.algo;

import buet.touhiDroid.MLOffline1.models.DecisionTree;
import buet.touhiDroid.MLOffline1.utils.Lg;

import java.util.ArrayList;

/**
 * Created by touhid on 11/27/15.
 *
 * @author touhid
 */
public class DecStumpAlgo {

    private static void printTreeStatus(DecisionTree decTree) {
        ArrayList<DecisionTree> childDecTrees = decTree.getChildDecTrees();
        if (childDecTrees.size() < 1)
            return;
        DecisionTree c = childDecTrees.get(0);
        Lg.pl("Node: " + decTree.getParentAttrib() + ", children: " + c.getParentAttrib());
        printTreeStatus(c);
    }
}
